/*
Testiä varten luotu autentikoinnin testiuser.
Oikeasti käyttäjät olisivat tietokannassa, josta voitaisiin hakea oikeaa käyttäjää.
Salasanat olisi tallennettu kantaan suolattuna ja yksisuuntaisesti kryptattuna.
Käyttäjän kirjautuessa tämän syöttämä salasana suolattaisiin käyttäjänimeen liitetyllä
suolalla ja kryptattaisiin, jotta voitaisiin verrata vastaavatko kryptatut arvot toisiaan.
*/

const user =
    {
        name: 'test1',
        password: 'salasana1'
    }

module.exports = user;