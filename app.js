//Haetaan moduulit käyttöön
var express = require('express');
var bodyParser = require('body-parser');
var jwt = require('express-jwt');

//Luodaan express appi
const app = express();

//Asetetaan express-jwt middlewareksi joka varmistaa että jwt token löytyy
//HUOM: secretin pitäisi olla jossain ulkoisessa tiedostossa, josta sitä kutsutaan appiin
app.use(jwt({ secret: 'salaisuus' }).unless({ path: ['/authenticate'] }));

//Asetetaan appi parsettamaan "application/x-www-form-urlencoded" sisällöt
app.use(bodyParser.urlencoded({ extended: true }))

//Asetetaan appi parsettamaan "application/json" sisällöt
app.use(bodyParser.json())

//Konfiguroidaan tietokanta
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.connect(dbConfig.url)
    .then(() => {
        console.log('Successfully connected to the database');
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now..');
        process.exit();
    });

//Haetaan reititykset
require('./routes/opiskelija.routes.js')(app);
require('./routes/signin.routes.js')(app);

//Asetetaan appi kuuntelemaan pyyntöjä
app.listen(3000, function () {
    console.log('app running on port. 3000');
});
